id = node.chipid()
m = mqtt.Client("IoT/"..id, 30)
m:connect("192.168.1.1", 1883, false, true)

function temp()
  status, temp, hum = dht.read(4)
  if (status == dht.OK) then
      print("Temp "..temp.." and hum "..hum)
      a=m:publish("/iot/"..id.."/temp", temp, 0, 0)
      b=m:publish("/iot/"..id.."/hum", hum, 0, 0)
      print(a,b)
  end
end

tmr.alarm(2, 5000, tmr.ALARM_AUTO, temp)
